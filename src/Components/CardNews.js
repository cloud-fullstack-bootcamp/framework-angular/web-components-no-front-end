class CardNews extends HTMLElement {

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: "open" });
        shadow.appendChild(this.build());
        shadow.appendChild(this.styles());
    }

    build() {
        const componentRoot = document.createElement("div");
        componentRoot.setAttribute("class", "card");

        const cardLeft = document.createElement("div");
        cardLeft.setAttribute("class", "card__left");

        const autor = document.createElement("span");
        autor.textContent = "By " + (this.getAttribute("autor") || "Anonymous");

        const linkTitle = document.createElement("a");
        linkTitle.textContent = this.getAttribute("title");
        linkTitle.href = this.getAttribute("link-url");

        const newContent = document.createElement("p");
        newContent.textContent = this.getAttribute("content");


        cardLeft.appendChild(autor);
        cardLeft.appendChild(linkTitle);
        cardLeft.appendChild(newContent);


        const cardRigth = document.createElement("div");
        cardRigth.setAttribute("class", "card__rigth");

        const newsImage = document.createElement("img");
        newsImage.src = this.getAttribute("photo") || "assets/avatar.png";
        newsImage.alt = "Foto da noticia";
        cardRigth.appendChild(newsImage);


        componentRoot.appendChild(cardLeft);
        componentRoot.appendChild(cardRigth);

        return componentRoot;
    }

    styles() {
        const style = document.createElement("style");
        style.textContent = `
        .card {
            width: 80%;
            /* border: 1px solid gray; */
            box-shadow: 6px 5px 8px -4px rgba(0, 0, 0, 0.52);
            -webkit-box-shadow: 6px 5px 8px -4px rgba(0, 0, 0, 0.52);
            -moz-box-shadow: 6px 5px 8px -4px rgba(0, 0, 0, 0.52);
            display: flex;
            flex-direction: row;
            justify-content: space-between;
        }
        
        .card__left {
            display: flex;
            flex-direction: column;
            justify-content: center;
            padding-left: 10px;
        }
        
        .card__left>span {
            font-weight: 400;
        }
        
        .card__left>a {
            margin-top: 15px;
            font-size: 25px;
            color: black;
            text-decoration: none;
            font-weight: 400;
        }
        
        .card__left>p {
            color: gray;
        }
        `;
        return style;
    }
}

customElements.define('card-news', CardNews);